package com.jimenez.botonbar;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class FragmentoEquipos extends Fragment {



    public FragmentoEquipos() {
        // Required empty public constructor
    }


    public static FragmentoEquipos newInstance(String param1, String param2) {
        FragmentoEquipos fragment = new FragmentoEquipos();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragmento_equipos, container, false);
    }
}