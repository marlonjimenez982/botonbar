package com.jimenez.botonbar;

import static android.content.ContentValues.TAG;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {

    //Paso 3 los demas estan en drawable y menu
    FrameLayout contenedorFragmentos;
    BottomNavigationView menuinferior;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menuinferior= findViewById(R.id.menuinferior);

        //Paso 4 capturar el evento con el objeto que implemente navigetion view
        menuinferior.setOnItemSelectedListener(
                new NavigationBarView.OnItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        //Programar lo que sucede cuando se da clic en el item calculadora, mapa o equipo
                        //Paso5

                        int id= item.getItemId();
                        Log.d(TAG, String.valueOf(id));

                        switch (id){

                            case R.id.Calcul:
                                // Paso6 Cargar calculadora
                                FragmentManager miAdministradorCal;
                                FragmentTransaction miTransaccionCal;

                                miAdministradorCal=getSupportFragmentManager();
                                miTransaccionCal=miAdministradorCal.beginTransaction();
                                miTransaccionCal.replace(R.id.contenedorfragmentos, new FragmentoCalculadora());
                                miTransaccionCal.commit();
                                break;

                            case R.id.Mapa:
                                //Cargar mapa
                                FragmentManager miAdministradorMap;
                                FragmentTransaction miTransaccionMap;

                                miAdministradorMap=getSupportFragmentManager();
                                miTransaccionMap=miAdministradorMap.beginTransaction();
                                miTransaccionMap.replace(R.id.contenedorfragmentos, new FragmentoMapa());
                                miTransaccionMap.commit();
                                break;

                            case R.id.Equipos:
                                //Cargar equipos
                                FragmentManager miAdministradorEquipo;
                                FragmentTransaction miTransaccionEquipo;

                                miAdministradorEquipo=getSupportFragmentManager();
                                miTransaccionEquipo=miAdministradorEquipo.beginTransaction();
                                miTransaccionEquipo.replace(R.id.contenedorfragmentos, new FragmentoEquipos());
                                miTransaccionEquipo.commit();
                                break;
                        }

                        return false;


                    }
                }


        );

    }
}