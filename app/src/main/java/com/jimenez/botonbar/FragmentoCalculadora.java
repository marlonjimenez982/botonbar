package com.jimenez.botonbar;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import org.mariuszgromada.math.mxparser.Expression;

import kotlin.contracts.Returns;


public class FragmentoCalculadora extends Fragment implements View.OnClickListener {

    //Paso7
    MaterialButton botonC, botonParenAbi, botonParenCerra, botonMultiplicar,
            boton7, boton8, boton9, botonDividir,
            boton4, boton5, boton6, botonSumar,
            boton1, boton2, boton3, botonRestar,
            botonAC, boton0, botonPunto, botonIgual;
    TextView expresion, resultado;

    public FragmentoCalculadora() {
        // Required empty public constructor
    }



    public static FragmentoCalculadora newInstance(String param1, String param2) {
        FragmentoCalculadora fragment = new FragmentoCalculadora();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Paso8
        View view= inflater.inflate(R.layout.fragment_fragmento_calculadora, container, false);

        botonC=view.findViewById(R.id.boton_C);
        botonAC=view.findViewById(R.id.boton_AC);
        botonParenAbi=view.findViewById(R.id.boton_parenAbi);
        botonParenCerra=view.findViewById(R.id.boton_parencerra);
        botonMultiplicar=view.findViewById(R.id.boton_multiplicacion);
        botonDividir=view.findViewById(R.id.boton_dividir);
        botonSumar=view.findViewById(R.id.boton_sumar);
        botonRestar=view.findViewById(R.id.boton_restar);
        botonIgual=view.findViewById(R.id.boton_igual);
        botonPunto=view.findViewById(R.id.boton_punto);
        boton1=view.findViewById(R.id.boton_Uno);
        boton2=view.findViewById(R.id.boton_Dos);
        boton3=view.findViewById(R.id.boton_Tres);
        boton4=view.findViewById(R.id.boton_Cuatro);
        boton5=view.findViewById(R.id.boton_Cinco);
        boton6=view.findViewById(R.id.boton_Seis);
        boton7=view.findViewById(R.id.boton_Siete);
        boton8=view.findViewById(R.id.boton_Ocho);
        boton9=view.findViewById(R.id.boton_Nueve);
        boton0=view.findViewById(R.id.boton_Cero);
        expresion=view.findViewById(R.id.expresion);
        resultado=view.findViewById(R.id.resultado);

        //3-escuchar
        botonParenAbi.setOnClickListener(this);
        botonParenCerra.setOnClickListener(this);
        botonPunto.setOnClickListener(this);
        boton1.setOnClickListener(this);
        boton2.setOnClickListener(this);
        boton3.setOnClickListener(this);
        boton4.setOnClickListener(this);
        boton5.setOnClickListener(this);
        boton6.setOnClickListener(this);
        boton7.setOnClickListener(this);
        boton8.setOnClickListener(this);
        boton9.setOnClickListener(this);
        boton0.setOnClickListener(this);
        botonC.setOnClickListener(this);
        botonAC.setOnClickListener(this);
        botonIgual.setOnClickListener(this);
        botonRestar.setOnClickListener(this);
        botonSumar.setOnClickListener(this);
        botonDividir.setOnClickListener(this);
        botonMultiplicar.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {

        MaterialButton boton=(MaterialButton) view;
        String texto;
        texto= boton.getText().toString();
        //this.expresion.setText(texto); //Parte1 sin concatenar
        String cadena;
        cadena= expresion.getText().toString();

        //Validar las teclas C o AC para el borrado correspondiente
        if (texto.equals("C")){
            expresion.setText("0");
            return;
        }
        if (texto.equals("AC")){
            expresion.setText("0");
            resultado.setText("0");
            return;
        }

        //Eliminar el cero inicial
        if (texto.equals(".")){
        }else if (cadena.equals("0")){
            cadena="";
        }

        if (texto.equals("=")){
            Expression analizador= new Expression(cadena);
            Double resulta= analizador.calculate();
            if (Double.isNaN(resulta)){
                resultado.setText("Corregir cálculo!");
            }else{
                resultado.setText(Double.toString(resulta));
            }
        }else {
            //concatenar lo digitado
            cadena = cadena + texto;
            expresion.setText(cadena);
        }

    }
}