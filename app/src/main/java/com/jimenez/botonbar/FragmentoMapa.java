package com.jimenez.botonbar;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;


public class FragmentoMapa extends Fragment {

    MapView mapa;
    IMapController mapControlador;
    MyLocationNewOverlay miubicacion;

    public FragmentoMapa() {
        // Required empty public constructor
    }


    public static FragmentoMapa newInstance(String param1, String param2) {
        FragmentoMapa fragment = new FragmentoMapa();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_fragmento_mapa, container, false);

        /*
        mapa=(MapView) view.findViewById(R.id.mapa);
        Context ctx  = getContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx.getApplicationContext()));

        mapa.setTileSource(TileSourceFactory.MAPNIK);
        mapa.setMultiTouchControls(true);

        String[] permisos= new String[]{

                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            requestPermissions(permisos,1);
        }

         */




        return view;
    }

}